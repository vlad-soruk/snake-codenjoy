package com.codenjoy.dojo.snake.model;

import java.util.Objects;

public class Cell {
    private int x;
    private int y;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public double getDistance(int X, int Y) {
        double distance;
        double xDif = Math.abs(X - x);
        double yDif = Math.abs(Y - y);
        distance = xDif + yDif;
        return distance;
    }

    public int getX() { return x; }
    public int getY() { return y; }

    public void setX(int X) { x = X; }
    public void setY(int Y) { y = Y; }

    @Override
    public String toString() {
        return "Cell{" + "x=" + x +
                ", y=" + y +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cell cell = (Cell) o;
        return x == cell.x && y == cell.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
