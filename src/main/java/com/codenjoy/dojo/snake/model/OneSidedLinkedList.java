package com.codenjoy.dojo.snake.model;

public class OneSidedLinkedList<E> {
    public static class Node<E>{
        private final E element;
        private Node<E> next;

        public Node(E e, Node<E> n) {
            element = e;
            next = n;
        }
        public E getElement() {
            return element;
        }
        public Node<E> getNext() {
            return next;
        }
        public void setNext(Node<E> n) {
            next = n;
        }
    }

    private Node<E> head = null;
    private Node<E> tail = null;
    private int size = 0;
    public OneSidedLinkedList() {}

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public Node<E> getHead() {
        return head;
    }
    public Node<E> getTail() {
        return tail;
    }
    public void setSize(int n) {
        size = n;
    }

    // Creates head of OneSidedLinkedList
    public void addFirst(E e){
        head = new Node<>(e, head);
        if (size == 0)
            tail = head;
        size++ ;
    }
    public void restartList() {
        head.setNext(null);
        head = null;
        tail = null;
        size = 0;
    }
    public void addAfter(E e, Node<E> previous) {
        Node<E> newest;
        if(previous.getNext() != null) {
            newest = new Node<>(e,previous.getNext());
        } else {
            newest = new Node<>(e,null);
            tail = newest;
        }
        previous.setNext(newest);
        size++;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("(SNAKE HEAD: ");
        sb.append(head.getElement());
        OneSidedLinkedList.Node<E> walk = head;
        while (walk.getNext() != null) {
            sb.append(walk.getElement());
            walk = walk.getNext();
            if (walk != tail)
                sb.append(", ");
            else  {
                sb.append(", TAIL: ").append(tail.getElement());
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
