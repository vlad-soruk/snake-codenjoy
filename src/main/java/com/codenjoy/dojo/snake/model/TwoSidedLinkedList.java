package com.codenjoy.dojo.snake.model;

public class TwoSidedLinkedList<E> {
    public static class Node<E> {
        private final E element;
        private Node<E> prev;
        private Node<E> next;
        public Node(E e, Node<E> p, Node<E> n) {
            element = e;
            prev = p;
            next = n;
        }
        public E getElement() {
            return element;
        }
        public Node<E> getPrev() {
            return prev;
        }
        public Node<E> getNext() {
            return next;
        }
        public void setPrev(Node<E> p) {
            prev = p;
        }
        public void setNext(Node<E> n) {
            next = n;
        }
    }
    private Node<E> header;  // First node in TwoSidedLinkedList
    private Node<E> trailer; // Last node in TwoSidedLinkedList
    private int size = 0;    // Number of elements in the list

    public TwoSidedLinkedList() {}

    public int size() {
        return size;
    }
    public boolean isEmpty() {
        return size == 0;
    }
    public Node<E> getHeader() {
        return header;
    }
    public Node<E> getTrailer() {
        return trailer;
    }

    // Creates header of TwoSidedLinkedList
    public void addFirst(E e) {
        header = new Node<>(e, null, header);
        if (size == 0)
            trailer = header;
        size++;
    }
    public void removeLast() {
        if (isEmpty()) return;
        remove(trailer);
    }
    public void newHead(E e, Node<E> before) {
        header = new Node<>(e, null, before);
        before.setPrev(header);
        size++;
    }

    private void remove(Node<E> node) {
        Node<E> predecessor = node.getPrev();
        Node<E> successor = node.getNext();
        if(predecessor != null)
            predecessor.setNext(successor);
        else
            header = successor;
        if(successor != null)
            successor.setPrev(predecessor);
        else
            trailer = predecessor;
        size--;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("(PATH HEAD: ");
        Node<E> walk = header;
        while (walk != trailer) {
            sb.append(walk.getElement());
            walk = walk.getNext();
            if (walk != trailer)
                sb.append(", ");
            else {
                sb.append(", PATH TAIL : ").append(walk.getElement());
            }
        }
        sb.append(")");
        return sb.toString();
    }
}
