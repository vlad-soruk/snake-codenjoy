package com.codenjoy.dojo.snake.experiments;

public class Cell3 {
    // coordinates
    public int y, x;
    // parent cell for path
    public Cell3 parent;
    // Heuristic cost of the current cell
    public int heuristicCost;
    // Final cost
    public int finalCost; // G + H with
    // G(n) the cost of the path from the start node to n
    // and H(n) the heuristic that estimates the cost of the cheapest path from n to the goal
    public boolean solution; // if cell is part of the solution path

    public Cell3(int y, int x) {
        this.y = y;
        this.x = x;
    }

    @Override
    public String toString() {
        return "Cell{" + "x=" + x +
                ", y=" + y +
                '}';
    }
}
