package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.RandomDice;
import com.codenjoy.dojo.snake.model.Cell;

import java.util.List;

/**
 * User: Vlad Soruk
 */
public class SnakeSolver implements Solver<Board> {

    private Dice dice;
    private static Board board;

    private static SnakeAI snakeAI;
    private static boolean snakeAICreated = false;

    public SnakeSolver(Dice dice) {
        this.dice = dice;
    }

    @Override
    public String get(Board board) {
        this.board = board;
        return snakeDirection();
    }

    public static String snakeDirection(){
        Point applePoint = board.getApples().get(0);
        Point snakePoint = board.getSnake().get(0);

        int destinationXCoordinate = applePoint.getX();
        int destinationYCoordinate = applePoint.getY();

        int startXCoordinate = snakePoint.getX();
        int startYCoordinate = snakePoint.getY();

        List<Point> snakeBody = board.getSnake();
        Cell snakeHead = new Cell(startXCoordinate, startYCoordinate);

        if (snakeAICreated) {
            snakeAI.updateSnakePosition(snakeHead, destinationXCoordinate, destinationYCoordinate, snakeBody, board.getStones().get(0));
        }
        else {
            System.out.println(" if (!snakeAICreated) worked");
            snakeAI = new SnakeAI();
            snakeAI.updateSnakePosition(snakeHead, destinationXCoordinate, destinationYCoordinate, snakeBody, board.getStones().get(0));
            snakeAICreated = true;
        }

        return snakeAI.getDirectionForSnake(snakeHead);
    }

    public static void main(String[] args) {
        WebSocketRunner.runClient(
                "http://164.90.168.158/codenjoy-contest/board/player/4x7leocreghcbto5pp45?code=1776445517589811252",
                new SnakeSolver(new RandomDice()),
                new Board());
    }
}