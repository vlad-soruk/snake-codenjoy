package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.model.Cell;
import com.codenjoy.dojo.snake.model.TwoSidedLinkedList;
import com.codenjoy.dojo.snake.model.OneSidedLinkedList;

import java.util.*;
import java.util.List;

public class SnakeAI {
    // TwoSidedLinkedList<Cell> path consists of nodes that
    // have access to next node and previous node
    TwoSidedLinkedList<Cell> path = new TwoSidedLinkedList<>();

    // OneSidedLinkedList<Cell> snake consists of nodes that
    // have access only to next node, but not to previous node
    OneSidedLinkedList<Cell> snake = new OneSidedLinkedList<>();
    boolean looking = false;

    SnakeAI() {}
    public void updateSnakePosition(Cell snakeHead, int destinationX, int destinationY, List<Point> snakeBody, Point stone) {
        if(snake.size() > snakeBody.size() && snakeBody.size() == 2) {
            // this code will only work when the game is over
            // (when algorithm hasn`t found a path to the apple, so the snake will
            // wander till the last remained possible cell in TwoSidedLinkedList<Cell> path)
            reset();
        }

        if(snake.isEmpty()) {
            // this code works in 2 cases:
            // 1. If SnakeSolver runs for the first time and constructor
            // publicSnakeAI() {} is triggered.
            // 2. When private void reset() method worked
            snake.addFirst(new Cell(snakeHead.getX(), snakeHead.getY())); // creates head of OneSidedLinkedList<Cell> snake
            snake.addAfter(new Cell(snakeBody.get(1).getX(), snakeBody.get(1).getY()), snake.getHead()); // creates tail of OneSidedLinkedList<Cell> snake
            AStarAlgorithm(snakeHead, destinationX, destinationY, stone);
            return;
        }

        Cell head = snake.getHead().getElement();
        int headXCoord = head.getX();
        int headYCoord = head.getY();
        head.setX(snakeHead.getX());
        head.setY(snakeHead.getY());

        path.removeLast();

        //TRAVERSE THE LIST AND UPDATE THE REST OF THE SNAKES POSITION
        OneSidedLinkedList.Node<Cell> node = snake.getHead().getNext();
        while(node != null) {
            int xCoord = node.getElement().getX();
            int yCoord = node.getElement().getY();
            node.getElement().setX(headXCoord);
            node.getElement().setY(headYCoord);
            headXCoord = xCoord;
            headYCoord = yCoord;
            node = node.getNext();
        }

        System.out.printf("\nsnake.size() in updateSnakePosition method: %s", snake.size());
        System.out.printf("\nONESIDEDLINKEDLIST in updateSnakePosition method!!! : %s", snake);
        System.out.printf("\nsnakeBody.size() in updateSnakePosition method: %s", snakeBody.size());
        if(snake.size() == snakeBody.size() - 1) {
            System.out.println("\nSNAKE COLLIDES WITH DESTINATION");
            Cell newCell = new Cell(snake.getTail().getElement().getX(), snake.getTail().getElement().getY());
            snake.addAfter(newCell, snake.getTail());
            AStarAlgorithm(snakeHead, destinationX, destinationY, stone);
        }
    }

    private void AStarAlgorithm(Cell snakeHead, int destinationX, int destinationY, Point stone) {
        // this code works in 2 cases:
        // 1. The game has just begun and snake body has only 2 cells of the board.
        // 2. At the moment when snake ate an apple, has become bigger and new apple
        // randomly appeared on the board.
        System.out.println("AStarAlgorithm is working!!!");
        // Empty TwoSidedLinkedList<Cell> is created
        path = new TwoSidedLinkedList<>();
        System.out.printf("\nONESIDEDLINKEDLIST in AStarAlgorithm method!!! : %s", snake);
        // Cell, where the head of the snake is located, is added to the path as its
        // initial point, from where algorithm should find path to the apple
        path.addFirst(snakeHead);
        // Here we will store cells, which will cause snakeCollision,
        // pathCollision or stoneCollision
        ArrayList<Cell> cracks = new ArrayList<>();
        looking = true;
        int steps = 1;
        while(looking) {
            // Initially current will be the cell that we added when wrote
            // path.addFirst(snakeHead), so it will be the head of the snake
            TwoSidedLinkedList.Node<Cell> current = path.getHeader();
            // toAdd is an adjacent cell, which will not cause snakeCollision,
            // crackCollision, pathCollision or stoneCollision, and, simultaneously,
            // has the least distance to the apple among other adjacent cells
            Cell toAdd = getClosest(current.getElement(), cracks, steps, destinationX, destinationY, stone);
            while(toAdd == null) {
                System.out.println("null " + steps);
                // If the algorithm cannot find available adjacent cell to the current cell,
                // this current cell will be added to ArrayList<Cell> cracks
                cracks.add(current.getElement());
                // We do step back from the current cell, to which the algorithm cannot
                // find available adjacent cell, and we will then again trigger getClosest
                // method on a node, which was the head of TwoSidedLinkedList<Cell> path
                // before the current cell
                current = current.getNext();
                steps -= 1;
                if(steps < 1) {
                    // This means that algorithm cannot find any available path to the apple
                    System.out.println("exited");
                    looking = false;
                    break;
                }
                toAdd = getClosest(current.getElement(), cracks, steps, destinationX, destinationY, stone);
            }

            if(toAdd != null) {
                // If there is available adjacent cell, we make it the head of
                // TwoSidedLinkedList<Cell> path
                path.newHead(toAdd, current); // previous head of path will now be next to the new head of path
                System.out.printf("\nPATH TWOSIDEDLINKEDLIST in AStarAlgorithm: %s", path.toString());
                steps += 1;
                // If the algorithm has found path to the apple, we break cycle 'while'
                if(toAdd.getX() == destinationX && toAdd.getY() == destinationY) looking = false;
            }
        }
        System.out.println("PATHFINDING ALGORITHM FINISHED!!!");
        System.out.println("Path Size: " + steps);
    }

    private Cell getClosest(Cell current, ArrayList<Cell> cracks, int steps, int destinationX, int destinationY, Point stone) {
        Cell closestCell = null;

        // Analyze 4 cells (from left, right, top and bottom) to the current cell,
        // if they don`t provoke snakeCollision, pathCollision, crackCollision or
        // stoneCollision. And analyze distance from current cell to the apple,
        // for instance, if destination has coordinates (x=1, y=13) and source (start
        // point) has coordinates (x=13, y = 1), then each cell will have the following
        // distance (heuristic cost = Math.abs(x - destinationXCoord) + Math.abs(y - destinationYCoord)):
        // DE 1  2  3  4  5  6  7  8  9  10 11 12
        // 1  2  3  4  5  6  7  8  9  10 11 12 13
        // 2  3  4  5  6  7  8  9  10 11 12 13 14
        // 3  4  5  6  7  8  9  10 11 12 13 14 15
        // 4  5  6  7  8  9  10 11 12 13 14 15 16
        // 5  6  7  8  9  10 11 12 13 14 15 16 17
        // 6  7  8  9  10 11 12 13 14 15 16 17 18
        // 7  8  9  10 11 12 13 14 15 16 17 18 19
        // 8  9  10 11 12 13 14 15 16 17 18 19 20
        // 9  10 11 12 13 14 15 16 17 18 19 20 21
        // 10 11 12 13 14 15 16 17 18 19 20 21 22
        // 11 12 13 14 15 16 17 18 19 20 21 22 23
        // 12 13 14 15 16 17 18 19 20 21 22 23 SO
        for(int x = -1; x <= 1; x++) {
            for(int y = -1; y <= 1; y++) {
                if((x == 0 && y != 0) || (x != 0 && y == 0)) {
                    int xBound = current.getX() + x;
                    int yBound = current.getY() + y;
                    if((xBound > 0 && xBound <= 13) && (yBound > 0 && yBound <= 13)) {
                        Cell cellToCheck = new Cell(xBound, yBound);
                        if(!snakeCollision(cellToCheck, steps) && !pathCollision(cellToCheck) && !crackCollision(cellToCheck, cracks) && !stoneCollision(cellToCheck, stone)) {
                            if(closestCell == null) {
                                closestCell = cellToCheck;
                            }
                            else if(cellToCheck.getDistance(destinationX, destinationY) <= closestCell.getDistance(destinationX, destinationY)) {
                                closestCell = cellToCheck;
                            }
                        }
                    }
                }
            }
        }
        return closestCell;
    }

    private boolean snakeCollision(Cell head, int steps) {
        OneSidedLinkedList.Node<Cell> node = snake.getHead();
        int count = 0;
        // We should consider that while snake is moving, its tail will also move,
        // so, for example, if snake.size() == 30 and steps == 1,
        // we will check collision of head with all cells of snake, but if
        // snake.size() == 30 and steps == 29 (it means that snake has already made 29
        // moves), we will only check possible collision
        // with snake.getHead().getNext().getElement();
        while(node != snake.getTail() && count < snake.size() - steps) {
            if((head.getX() == node.getElement().getX()) && (head.getY() == node.getElement().getY())) {
                return true;
            }
            node = node.getNext();
            count++;
        }
        return false;
    }

    private boolean pathCollision(Cell head) {
        TwoSidedLinkedList.Node<Cell> node = path.getHeader();
        while(node != null) {
            if((head.getX() == node.getElement().getX()) && (head.getY() == node.getElement().getY())) {
                return true;
            }
            node = node.getNext();
        }
        return false;
    }

    private boolean crackCollision(Cell head, ArrayList<Cell> cracks) {
        for (Cell crack : cracks) {
            if (head.getX() == crack.getX() && head.getY() == crack.getY()) {
                return true;
            }
        }
        return false;
    }

    private boolean stoneCollision(Cell head, Point stone) {
        return head.getX() == stone.getX() && head.getY() == stone.getY();
    }

    public String getDirectionForSnake(Cell snakeHead) {
        Cell next = null;
        if(path.getTrailer() != null) {
            if(path.getTrailer().getPrev() != null) {
                // We take path.getTrailer().getPrev().getElement() as the next
                // cell for move, because path.getTrailer() will always contain
                // cell, where snakeHead is located
                next = path.getTrailer().getPrev().getElement();
                System.out.printf("\npath.getTrailer().getPrev() != null: %s", path.getTrailer().getPrev().getElement());
            }
            else {
                // path.getTrailer() can be null at the moment when snake did all moves
                // from TwoSidedLinkedList<Cell> path and the game is over, because
                // algorithm hasn`t found path to the apple. Thus, we tell snake to
                // collide with the cell, before snake`s head. Because of this, snake`s
                // behavior in the end of the game is always the same.
                next = snake.getHead().getNext().getElement();
                System.out.printf("\npath.getTrailer().getPrev() == null: %s", next);
            }
        }
        System.out.printf("\nNextCell!!! : %s", next);
        System.out.printf("\nSNAKEHEAD !!! : %s", snakeHead);

        return getDirection(snakeHead, next);
    }

    private String getDirection(Cell snakeHead, Cell next) {
        String direction = Direction.RIGHT.toString();

        if (next != null) {
            if(next.getY() > snakeHead.getY() && next.getX() == snakeHead.getX()) {
                direction = Direction.UP.toString();
            }
            else if(next.getY() < snakeHead.getY() && next.getX() == snakeHead.getX()) {
                direction = Direction.DOWN.toString();
            }
            else if(next.getY() == snakeHead.getY() && next.getX() > snakeHead.getX()) {
                direction = Direction.RIGHT.toString();
            }
            else if(next.getY() == snakeHead.getY() && next.getX() < snakeHead.getX()) {
                direction = Direction.LEFT.toString();
            }
        }
        return direction;
    }

    private void reset() {
        System.out.println("RESTART IS WORKING!!!");
        // We fully clean OneSidedLinkedList<Cell> snake, so it will be empty
        snake.restartList();
    }
}
