package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.snake.client.Board;
import com.codenjoy.dojo.snake.experiments.Cell3;

import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;









// ----- EXPERIMENTAL CODE ----- //














public class AStarAlgorithm {
    // cost for diagonal and vertical / horizontal moves
    // public static final int DIAGONAL_COST = 14;
    public static final int V_H_COST = 10;
    public static final Stack<Cell3> directionsSolution = new Stack<>();
    private static Cell3 current = new Cell3(0, 0);

    // cells of out grid
    private Cell3[][] grid;
    //    We define a priority queue for open cells
    //    Open Cells: the set of nodes to be evaluated
    //    We put cells with lowest cost in first
    private PriorityQueue<Cell3> openCells;
    //    Closed cells: the set of nodes that are already evaluated
    private boolean[][] closedCells;
    //    Start cell
    private int startYCoordinate, startXCoordinate;
    //    End cell
    private int endYCoordinate, endXCoordinate;

    public AStarAlgorithm(Board board, int sYCoord, int sXCoord, int endYCoord, int endXCoord, List<Point> blockedCells) {
        grid = new Cell3[board.size() - 2][board.size() - 2];
        closedCells = new boolean[board.size() - 2][board.size() - 2];
        openCells = new PriorityQueue<>( (Cell3 c1, Cell3 c2) -> {
            return c1.finalCost < c2.finalCost ? -1 : c1.finalCost > c2.finalCost ? 1 : 0;
        } );

        startCell(sYCoord, sXCoord);
        endCell(endYCoord, endXCoord);

//        init heuristic and cells
        for (int y = 0; y < grid.length; y++) {
            for (int x = 0; x < grid[y].length; x++) {
                grid[y][x] = new Cell3(y, x);
                grid[y][x].heuristicCost = Math.abs(y - endYCoordinate) + Math.abs(x - endXCoordinate);
//                System.out.printf("%.3f   ", Math.sqrt(Math.pow((y - endI), 2) + Math.pow((x - endJ), 2)));
//                grid[y][x].heuristicCost = (int) Math.sqrt(Math.pow((y - endI), 2) + Math.pow((x - endJ), 2));
                grid[y][x].solution = false;
            }
//            System.out.println();
        }

        grid[startYCoordinate][startXCoordinate].finalCost = 0;

//            We put the blocks on the grid
        for (int i = 0; i < blockedCells.size(); i++) {
            addBlockOnCell( board.inversionY(blockedCells.get(i).getY()) - 1, blockedCells.get(i).getX() - 1);
        }
    }

    public void startCell(int y, int x) {
        startYCoordinate = y;
        startXCoordinate = x;
    }

    public void endCell(int y, int x) {
        endYCoordinate = y;
        endXCoordinate = x;
    }

    public void addBlockOnCell(int y, int x) {
        grid[y][x] = null;
    }

    public void updateCostIfNeeded(Cell3 current, Cell3 adjacentCell, int cost) {
        if(adjacentCell == null || closedCells[adjacentCell.y][adjacentCell.x]) return;

        int adjacentCellFinalCost = adjacentCell.heuristicCost + cost;
        boolean isOpen = openCells.contains(adjacentCell);

        if (!isOpen || adjacentCellFinalCost < adjacentCell.finalCost) {
            adjacentCell.finalCost = adjacentCellFinalCost;
            adjacentCell.parent = current;

            if (!isOpen) openCells.add(adjacentCell);
        }
    }

    public void process() {
        // we add the start location to open list
        System.out.println("\ngrid[startYCoordinate][startXCoordinate] ");
        System.out.println(grid[startYCoordinate][startXCoordinate]);
        openCells.add(grid[startYCoordinate][startXCoordinate]);
        System.out.println(openCells);

        Cell3 current;

        while(true) {
            // poll извлекает и удаляет наименьший элемент из очереди приоритетов
            current = openCells.poll();

            if (current == null) break;

            closedCells[current.y][current.x] = true;

            if ( current.equals(grid[endYCoordinate][endXCoordinate]) ) return;

            Cell3 adjacentCell;

            if (current.y - 1 >= 0) {
                adjacentCell = grid[current.y - 1][current.x];
                updateCostIfNeeded(current, adjacentCell, current.finalCost + V_H_COST);

//                if (current.j - 1 >= 0) {
//                    adjacentCell = grid[current.i - 1][current.j - 1];
//                    updateCostIfNeeded(current, adjacentCell, current.finalCost + DIAGONAL_COST);
//                }
//
//                if (current.j + 1 < grid[0].length) {
//                    adjacentCell = grid[current.i - 1][current.j + 1];
//                    updateCostIfNeeded(current, adjacentCell, current.finalCost + DIAGONAL_COST);
//                }
            }

            if (current.x - 1 >= 0) {
                adjacentCell = grid[current.y][current.x - 1];
                updateCostIfNeeded(current, adjacentCell, current.finalCost + V_H_COST);
            }

            if (current.x + 1 < grid[0].length) {
                adjacentCell = grid[current.y][current.x + 1];
                updateCostIfNeeded(current, adjacentCell, current.finalCost + V_H_COST);
            }

            if (current.y + 1 < grid.length) {
                adjacentCell = grid[current.y + 1][current.x];
                updateCostIfNeeded(current, adjacentCell, current.finalCost + V_H_COST);

//                if (current.j - 1 >= 0) {
//                    adjacentCell = grid[current.i + 1][current.j - 1];
//                    updateCostIfNeeded(current, adjacentCell, current.finalCost + DIAGONAL_COST);
//                }
//
//                if (current.j + 1 < grid[0].length) {
//                    adjacentCell = grid[current.i + 1][current.j + 1];
//                    updateCostIfNeeded(current, adjacentCell, current.finalCost + DIAGONAL_COST);
//                }
            }
        }
    }

    public void display() {
        System.out.println("Grid:");

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (i == startYCoordinate && j == startXCoordinate) {
                    System.out.print("SO "); // Source cell
                }
                else if (i == endYCoordinate && j == endXCoordinate) {
                    System.out.print("DE "); // Destination cell
                }
                else if (grid[i][j] != null) {
                    System.out.printf("%-3d", grid[i][j].heuristicCost);
                }
                else {
                    System.out.print("BL "); // Block cell
                }
            }

            System.out.println();
        }

        System.out.println();
    }

    public void displayScores() {
        System.out.println("\nScores for cells: ");

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] != null) System.out.printf("%-3d ", grid[i][j].finalCost);
                else System.out.print("BL  ");
            }

            System.out.println();
        }

        System.out.println();
    }

    public void displaySolution() {
        if (closedCells[endYCoordinate][endXCoordinate]) {
            // We track back the path
            System.out.println("PAAATHHHH!!!: ");
            Cell3 current = grid[endYCoordinate][endXCoordinate];

            directionsSolution.push(current);

            grid[current.y][current.x].solution = true;

            while(current.parent != null) {
                directionsSolution.push(current.parent);
                System.out.print(" -> " + current.parent);
                grid[current.parent.y][current.parent.x].solution = true;
                current = current.parent;
            }

            System.out.println("\n");

            for (int i = 0; i < grid.length; i++) {
                for (int j = 0; j < grid[i].length; j++) {
                    if (i == startYCoordinate && j == startXCoordinate) {
                        System.out.print("SO "); // Source cell
                    }
                    else if (i == endYCoordinate && j == endXCoordinate) {
                        System.out.print("DE "); // Destination cell
                    }
                    else if (grid[i][j] != null) {
                        System.out.printf("%-3s", grid[i][j].solution ? "X" : "0");
                    }
                    else {
                        System.out.print("BL "); // Block cell
                    }
                }

                System.out.println();
            }

            System.out.println();
        } else {
            System.out.println("No possible path!");

        }
    }

    public static void main(String[] args) {
//        AStarAlgorithm aStar = new AStarAlgorithm(13, 13, 12, 12, 0, 0,
//                List<Point> blockedCells
//        );

//        AStarAlgorithm aStar = new AStarAlgorithm(13, 13, 12, 12, 0, 0,
//                new int[][]{
//                        {0, 4}, {2, 2}, {3, 1}, {3, 3}, {2, 1}, {2, 3},
//                }
//        );

//        aStar.display();
//        aStar.process();
//        aStar.displayScores();
//        aStar.displaySolution();
//        aStar.displayParents();
//        aStar.displayWithDirections();
    }

    public String getDirectionForSnake() {
//        System.out.println("Display with directions:");
        current = directionsSolution.pop();
        Cell3 next;
        String direction = null;

        if (!directionsSolution.isEmpty()) {
            next = directionsSolution.pop();
            if(next.y > current.y && next.x == current.x) {
                direction = Direction.DOWN.toString();
//                System.out.println("Direction DOWN!");
            }
            else if(next.y < current.y && next.x == current.x) {
                direction = Direction.UP.toString();
//                System.out.println("Direction UP!");
            }
            else if(next.y == current.y && next.x > current.x) {
                direction = Direction.RIGHT.toString();
//                System.out.println("Direction RIGHT!");
            }
            else {
                direction = Direction.LEFT.toString();
//                System.out.println("Direction LEFT!");
            }
            current = next;

//            else if(next.y > current.y && next.x < current.x) {
//                System.out.println("Direction DIAGONAL DOWN LEFT!");
//            }
//            else if(next.y > current.y && next.x > current.x) {
//                System.out.println("Direction DIAGONAL DOWN RIGHT!");
//            }
//            else if(next.y < current.y && next.x < current.x) {
//                System.out.println("Direction DIAGONAL UP LEFT!");
//            }
//            else {
//                System.out.println("Direction DIAGONAL UP RIGHT!");
//            }
        }
        return direction;
    }

    private void displayParents() {
        System.out.println("Cells parents: ");
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if(grid[i][j] == null) {
                    System.out.printf("%-20d ", 0);
                }
                else {
                    System.out.printf("%-20s ",grid[i][j].parent);
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}
